// eslint-disable-next-line
module.exports = {
    host     : "0.0.0.0",
    port     : 3000,
    settings : {
        easyMode   : { field: 5, delay: 1500 },
        normalMode : { field: 10, delay: 1000 },
        hardMode   : { field: 15, delay: 900 }
    }
};
